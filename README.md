你可能每天都会去看女朋友的当天气温如何，并天天都在嘘寒问暖；

可能某一天的你早上起来忘了这件事情，女朋友就会大闹说你不关心她了...

为了杜绝这种事情再次发生，夏柔给大家写了一个Python自动化推送生活早报的脚本～

当你拥有了这款Python每日早报脚本，你的女朋友一定会越来越爱上你！

（此处省略一万个营销文字....）

教程开始

阅读更多详细教程请前往：[https://www.wpon.cn/22504.html](https://www.wpon.cn/22504.html)

![输入图片说明](https://images.gitee.com/uploads/images/2021/1125/094707_0dee80ce_5391785.png "屏幕截图.png")
上面都弄好后，接下来弄腾讯云的相关配置信息：

![输入图片说明](https://www.wpon.cn/wp-content/uploads/2021/11/2021112406390170.png "在这里输入图片标题")
SecretId、SecretKey 获取入口：
https://console.cloud.tencent.com/cam/capi

SDKAppID 获取入口：
https://console.cloud.tencent.com/smsv2/app-manage

APPID 获取入口：
https://console.cloud.tencent.com/smsv2/csms-sign

短信模板 获取入口：
https://console.cloud.tencent.com/smsv2/csms-template

（称得上保姆级教学了...）

一切都配置好后，让我们看看实际效果：
![输入图片说明](https://images.gitee.com/uploads/images/2021/1125/094857_c1c1b36c_5391785.png "屏幕截图.png")

手机接收短信：
![输入图片说明](https://images.gitee.com/uploads/images/2021/1125/094913_08a5e849_5391785.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1125/094921_2b011971_5391785.png "屏幕截图.png")

其中，文化谚语API代码：

# 文化谚语

```
url = 'http://api.tianapi.com/proverb/index?key=你的密钥'
response = requests.get(url)
res = json.loads(response.text)
# res_2 = res.decode('utf-8')
yan_shangju = res['newslist'][0]['front']
yan_xiaju = res['newslist'][0]['behind']
```


生活指数：

# 生活指数

```
url = 'https://api.jisuapi.com/weather/query?appkey=你的密钥&city=沈阳'
response = requests.get(url)
res = json.loads(response.text)
# 获取当前城市
wpon_city = res['result']['city']
# 获取当前城市的温度
wpon_temp = res['result']['temp']
# 获取当前城市的穿衣指南
wpon_detail = res['result']['index'][6]['detail']
# 获取前12句
wpon_detail2 = wpon_detail[0:12]
```

部署教程

注：使用Pycharm工具，Python3.8版本

首先在终端打开当前项目路径，并输入：

```

pip install requests
```
![输入图片说明](https://images.gitee.com/uploads/images/2021/1125/095023_de05b98d_5391785.png "屏幕截图.png")

输入命令后出现上面的报错，则是pip命令版本需要升级，在终端输入：pip install --upgrade pip 即可

接下来输入腾讯云模块包命令：

```
pip install tencentcloud-sdk-python

```
![输入图片说明](https://images.gitee.com/uploads/images/2021/1125/095104_3d9c21c6_5391785.png "屏幕截图.png")


若图片中没有红色波浪线则代表所有模块包已经安装完毕；

接下来直接运行实例项目即可。

### 部署到云服务器教程


云服务器一般默认的Python版本为2.7，如果你不确定的话请输入指令：python -V 查看当前版本，如图：

![输入图片说明](https://images.gitee.com/uploads/images/2021/1125/095136_2d96e56e_5391785.png "屏幕截图.png")

切换 Python3.8 方法如下

首先执行以下命令


```
wget https://cdn.wpon.cn/python/Python-3.8.1.tgz && ls

```
![输入图片说明](https://images.gitee.com/uploads/images/2021/1125/095153_f71b8152_5391785.png "屏幕截图.png")

右边的 Python-3.8.1.tgz 复制到剪贴板，然后在命令行输入：
```
tar -zxvf Python-3.8.1.tgz
```

![输入图片说明](https://images.gitee.com/uploads/images/2021/1125/095225_1c73f512_5391785.png "屏幕截图.png")

接下来输入以下命令：

```
cd Python-3.8.1 && ./configure
```
![输入图片说明](https://images.gitee.com/uploads/images/2021/1125/095240_29195e29_5391785.png "屏幕截图.png")

然后输入 make 命令（有可能会慢一点，这个时候可以多看看上面的教程认真读一读）：

```
make
```
![输入图片说明](https://images.gitee.com/uploads/images/2021/1125/095259_64b03fef_5391785.png "屏幕截图.png")
最后输入 

```
make install
```

![输入图片说明](https://images.gitee.com/uploads/images/2021/1125/095341_a115dd45_5391785.png "屏幕截图.png")
然后直接复制粘贴以下命令到服务器上

```
mv /usr/bin/python /usr/bin/python22
ln -s /usr/local/bin/python3 /usr/bin/python
```
![输入图片说明](https://images.gitee.com/uploads/images/2021/1125/095406_c76550c0_5391785.png "屏幕截图.png")

现在可以看到我们的Python版本已经切换到了3.8～

现在，上传脚本，输入以下命令：
```
yum install lrzsz
```
![输入图片说明](https://images.gitee.com/uploads/images/2021/1125/095440_eceeffb1_5391785.png "屏幕截图.png")
提示图中报错，请进入 /usr/bin/yum 、/usr/libexec/urlgrabber-ext-down 文件中的第一行为#!/usr/bin/python2.7 即可解决

命令：vi /usr/bin/yum 、vi /usr/libexec/urlgrabber-ext-down

然后输入字母 i 进入编辑模式；

修改好后，按左上角esc键，并输入 :wq （注意有冒号）后回车即可，如图：
![输入图片说明](https://images.gitee.com/uploads/images/2021/1125/095502_471b3687_5391785.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/1125/095509_ce7e6ca7_5391785.png "屏幕截图.png")

现在可以看到，已经可以使用 yum 命令了

接下来输入上面 安装rz命令：

```
yum install lrzsz
```
安装好后，输入以下命令创建目录，并上传你的python项目，如图：

```
mkdir /pythonproject && cd /pythonproject && rz
```
![输入图片说明](https://images.gitee.com/uploads/images/2021/1125/095600_03fb1e0a_5391785.png "屏幕截图.png")

看一下文件是否已经上传了：
![z](https://images.gitee.com/uploads/images/2021/1125/095613_db8799dd_5391785.png "屏幕截图.png")

现在让我们尝试运行一下，输入以下命令并回车：
```
python weather-wpon.py
```
![输入图片说明](https://images.gitee.com/uploads/images/2021/1125/095636_965f36c2_5391785.png "屏幕截图.png")
啊哦，缺少 requests 模块包，输入 pip install requests 即可解决～

别忘记升级一下pip命令：

```
pip install --upgrade pip

```
然后再输入：
```
pip install requests
```
最后，再把腾讯云模块包安装上：

```
pip install tencentcloud-sdk-python
```
![输入图片说明](https://images.gitee.com/uploads/images/2021/1125/095732_2f1b676b_5391785.png "屏幕截图.png")
扩展小知识

WARNING: Running pip as the 'root' user can result in broken permissions and conflicting behaviour with the system package manager. It is recommended to use a virtual environment instead: https://pip.pypa.io/warnings/venv

上文黄色的警告只是提醒你创建一个虚拟环境去运行pip命令，否则容易搞乱系统环境，如果你是小白的话，这个可以完全忽略。

现在，惊人激动的时刻到了；

输入命令 python weather-wpon.py 回车看看效果：
![输入图片说明](https://images.gitee.com/uploads/images/2021/1125/095747_20954140_5391785.png "屏幕截图.png")
哦豁，发送成功～
![输入图片说明](https://images.gitee.com/uploads/images/2021/1125/095758_5d8ff380_5391785.png "屏幕截图.png")

如何实现自动发送短信推送？

输入以下命令：

```

yum install crontabs
 
systemctl enable crond
 
systemctl start crond
```

依次输入后，查看当前crond状态，如图：
![输入图片说明](https://images.gitee.com/uploads/images/2021/1125/095816_30417312_5391785.png "屏幕截图.png")

开始设置新任务，输入命令：vi /etc/crontab 并回车；

然后输入字母 i 进入编辑模式；
![输入图片说明](https://images.gitee.com/uploads/images/2021/1125/095827_1eed8a33_5391785.png "屏幕截图.png")
然后输入命令：
```
* 6 * * * cd /pythonproject && python weather-wpon.py

```
修改好后，按左上角esc键，并输入 :wq （注意有冒号）后回车即可，如上图
然后输入以下命令，并重启 cornd ：
```
crontab /etc/crontab
service crond start
```
查看当前任务命令：
```
crontab -l
```
![输入图片说明](https://images.gitee.com/uploads/images/2021/1125/095900_cb2e77cb_5391785.png "屏幕截图.png")
现在，每天凌晨6点推送短信早报已经大功告成～

这是夏柔今天早上6点收到的短信：
![输入图片说明](https://images.gitee.com/uploads/images/2021/1125/095909_305f6dff_5391785.png "屏幕截图.png")

如果需要设置其他时间段，请参考以下文档：

```
 .---------------- 分钟 (0 - 59) 
| .------------- 小时 (0 - 23) 
| | .---------- 每月的第几天 (1 - 31) 
| | | .------- 每年的几月 (1 - 12) OR jan,feb,mar,apr ... 
| | | | .---- 每周 (0 - 6) (Sunday=0 or 7) OR sun,mon,tue,wed,thu,fri,sat 
| | | | | 
```
设置什么时间发送，就设置哪块；
```
* 6 * * * 命令

```
最后，如有不懂的地方，记得联系夏柔QQ：15001904