# -*- coding:UTF-8 -*-
# 夏柔QQ：15001904
# 官方网站：www.wpon.cn
# 本源码使用教程：https://www.wpon.cn/22504.html
import requests,json
from tencentcloud.common import credential
from tencentcloud.common.exception.tencent_cloud_sdk_exception import TencentCloudSDKException
# 导入对应产品模块的client models。
from tencentcloud.sms.v20210111 import sms_client, models
# 导入可选配置类
from tencentcloud.common.profile.client_profile import ClientProfile
from tencentcloud.common.profile.http_profile import HttpProfile

# 文化谚语
url = 'http://api.tianapi.com/proverb/index?key=你的密钥'
response = requests.get(url)
res = json.loads(response.text)
# res_2 = res.decode('utf-8')
yan_shangju = res['newslist'][0]['front']
yan_xiaju = res['newslist'][0]['behind']

# 生活指数
url = 'https://api.jisuapi.com/weather/query?appkey=你的密钥&city=沈阳'
response = requests.get(url)
res = json.loads(response.text)
# 获取当前城市
wpon_city = res['result']['city']
# 获取当前城市的温度
wpon_temp = res['result']['temp']
# 获取当前城市的穿衣指南
wpon_detail = res['result']['index'][6]['detail']
# 获取前12句
wpon_detail2 = wpon_detail[0:12]
# print(wpon_detail2)
# print('宝，',r,'永远关心你的柔儿～')
try:
    cred = credential.Credential("你的SecretId", "你的SecretKey")
    httpProfile = HttpProfile()
    httpProfile.reqMethod = "POST"  # post请求(默认为post请求)
    httpProfile.reqTimeout = 30    # 请求超时时间，单位为秒(默认60秒)
    httpProfile.endpoint = "sms.tencentcloudapi.com"  # 指定接入地域域名(默认就近接入)
    clientProfile = ClientProfile()
    clientProfile.signMethod = "TC3-HMAC-SHA256"  # 指定签名算法
    clientProfile.language = "zh-CN"
    clientProfile.httpProfile = httpProfile
    client = sms_client.SmsClient(cred, "ap-guangzhou", clientProfile)
    req = models.SendSmsRequest()
    # 你的短信应用SDKAPPID
    req.SmsSdkAppId = "你的APPID"
    # 短信签名内容: 使用 UTF-8 编码，必须填写已审核通过的签名，签名信息可登录 [短信控制台] 查看
    req.SignName = "WordPress小闫"
    req.PhoneNumberSet = ["+8613333333333"]
    # 短信正文模板ID
    req.TemplateId = "你的模板ID"
    # 模板参数（切勿修改）
    req.TemplateParamSet = [wpon_city,wpon_temp,wpon_detail2,yan_shangju,yan_xiaju]
    resp = client.SendSms(req)
    print(resp.to_json_string(indent=2))
except TencentCloudSDKException as err:
    print(err)